const express = require('express');
const multer = require('multer');
const Cocktail = require('../models/Cocktail');
const config = require('../config');
const {nanoid} = require("nanoid");
const User = require("../models/User");
const auth = require("../middleware/auth");
const path = require("path");
const permit = require("../middleware/permit");

const router = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage})

router.get('/', async (req, res) => {
  try {

    const token = req.get('Authorization');
    const user = await User.findOne({token});
    let cocktails = null;
    if (user && user.role === 'admin') {
      cocktails = await Cocktail.find();
    } else if (req.query.personal) {
      cocktails = await Cocktail.find({user});
    } else {
      cocktails = await Cocktail.find({published: true});
    }
    res.send(cocktails);
  } catch (e) {
    console.log(e);
    res.sendStatus(500);
  }
});

router.get('/:id', async (req, res) => {
  try {
    const cocktail = await Cocktail.findById(req.params.id);

    if (cocktail) {
      res.send(cocktail);
    } else {
      res.status(404).send({error: 'Cocktail not found'});
    }
  } catch {
    res.sendStatus(500);
  }
});

router.post('/', auth, upload.single('image'), async (req, res) => {

  const token = req.get('Authorization');
  const user = await User.findOne({token});

  console.log(req.body);

  if (!req.body.title && !req.body.recipe && !req.body.ingredients) {
    return res.status(400).send('Data not valid');
  }

  const cocktailData = {
    title: req.body.title,
    user: user._id,
    recipe: req.body.recipe,
    ingredients: [req.body.ingredients],
  };

  if (req.file) {
    cocktailData.image = 'uploads/' + req.file.filename;
  }

  const cocktail = new Cocktail(cocktailData);

  try {
    await cocktail.save();
    res.send(cocktail);
  } catch (e) {
    res.status(400).send(e);
  }
});

router.post('/:id/publish', auth, upload.single('image'), permit('admin'), async (req, res) => {
  try {
    const cocktail = await Cocktail.findById(req.params.id);

    if (Object.keys(cocktail).length === 0) {
      return res.status(404).send({error: `Album ID=${req.params.id} is not found.`});
    } else {
      cocktail.published = !cocktail.published;
      await cocktail.save({validateBeforeSave: false});
      return res.send({message: `Cocktail ${cocktail.title} successfully published.`})
    }
  } catch (error) {
    res.status(404).send(error);
  }
});


router.delete('/:id', auth, permit('admin'), async (req, res) => {
  try {
    const cocktail = await Cocktail.findByIdAndRemove(req.params.id);

    if (cocktail) {
      res.send(`Cocktail '${cocktail.title} removed'`);
    } else {
      res.status(404).send({error: 'Cocktail not found'});
    }
  } catch (e) {
    console.log(e);
    res.sendStatus(500);
  }
});

module.exports = router;