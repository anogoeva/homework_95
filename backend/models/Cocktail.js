const mongoose = require('mongoose');
const idvalidator = require('mongoose-id-validator');


const IngredientSchema = new mongoose.Schema({
  title: String,
  amount: String,
})

const CocktailSchema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },
  title: {
    type: String,
    required: true,
  },
  image: String,
  recipe: {
    type: String,
    required: true,
  },
  published: {
    type: Boolean,
    default: false
  },
  ingredients: [IngredientSchema],
});

CocktailSchema.plugin(idvalidator);
const Cocktail = mongoose.model('Cocktail', CocktailSchema);
module.exports = Cocktail;