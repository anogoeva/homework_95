const mongoose = require('mongoose');
const {nanoid} = require('nanoid');
const config = require('./config');
const User = require("./models/User");
const Cocktail = require("./models/Cocktail");

const run = async () => {
  await mongoose.connect(config.db.url);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }


  const [admin, user] = await User.create(
    {
      username: 'admin',
      password: '123',
      token: nanoid(),
      role: 'admin',
      email: 'admin@admin.com',
      displayName: 'super admin',
    }, {
      username: 'user',
      password: '123',
      token: nanoid(),
      role: 'user',
      email: 'user@user.com',
      displayName: 'super user',
    }
  );

  await Cocktail.create(
    {
      user: admin,
      title: 'Дайкири',
      image: 'fixtures/dajkiri.jpg',
      recipe: 'Рецепт коктейля Дайкири · Налей в шейкер лаймовый сок 30 мл, сахарный сироп 15 мл и белый ром 60 мл · Наполни шейкер кубиками льда и взбей.',
      published: true,
      ingredients: [{title: 'лаймовый сок', amount: '50'}, {
        title: 'сахарный сироп',
        amount: '100'
      }, {title: 'белый ром', amount: 200}],
    },
    {
      user: admin,
      title: 'Мохито',
      image: 'fixtures/mohito.jpg',
      recipe: 'Мохито безалкогольный · мяту, тростниковый сахар и разминаем. · в бокал и заливаем спрайтом. · коктейль листиком мяты и лаймом — напиток готов.',
      ingredients: [{title: 'мята', amount: '50'}, {title: 'сахар', amount: 40}, {
        title: 'спрайт',
        amount: '35'
      }, {title: 'лайм', amount: 90}],
    },
    {
      user: user,
      title: 'Негрони',
      image: 'fixtures/negroni.jpg',
      recipe: 'Рецепт коктейля Негрони · Наполни рокс кубиками льда доверху · Налей в бокал красный вермут 30 мл и красный биттер 30 мл · Добавь джин 30 мл и размешай коктейльной',
      published: true,
      ingredients: [{title: 'лед', amount: '50'}, {title: 'вермут', amount: '450'}, {title: 'биттер', amount: 43}],
    },
    {
      user: user,
      title: 'Джин тоник',
      image: 'fixtures/djin-tonik.jpg',
      recipe: 'Рецепт коктейля Джин тоник · Наполни хайбол кубиками льда доверху · Налей джин 50 мл · Долей тоник доверху и аккуратно размешай коктейльной ложкой',
      ingredients: [{title: 'лед', amount: '100'}, {title: 'джин', amount: '100'}],
    },
  );

  await mongoose.connection.close();
};

run().catch(console.error);

