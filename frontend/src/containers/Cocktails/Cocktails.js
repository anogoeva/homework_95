import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Link, useLocation} from "react-router-dom";
import {Button, CircularProgress, Grid, makeStyles, Typography} from "@material-ui/core";
import CocktailItem from "../../components/CocktailItem/CocktailItem";
import CocktailLayout from "../../components/UI/Layout/CocktailLayout";
import {fetchCocktails} from "../../store/actions/cocktailsActions";
import ButtonWithProgress from "../../components/UI/ButtonWithProgress/ButtonWithProgress";
import {historyPush} from "../../store/actions/historyActions";

const useStyles = makeStyles(theme => ({
    title: {
        [theme.breakpoints.down('xs')]: {
            marginLeft: "50px",
        },
    }
}));

const Cocktails = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const cocktails = useSelector(state => state.cocktails.cocktails);
    const fetchLoading = useSelector(state => state.cocktails.fetchLoading);
    const user = useSelector(state => state.users.user);
    const search = useLocation().search;
    const loading = useSelector(state => state.users.loginLoading);


    useEffect(() => {
        dispatch(fetchCocktails(search));
    }, [dispatch, search]);

    const addCocktail = () => {
        dispatch(historyPush('/addCocktail'));
    };

    return (
        <CocktailLayout>
            <Grid container direction="column" spacing={2}>
                <Grid item container justifyContent="space-between" alignItems="center">
                    {user ?
                        <Grid item className={classes.title}>
                            <Typography variant="h4">Cocktails</Typography>
                        </Grid>
                        : ''}
                    {user ?
                        <ButtonWithProgress
                            onClick={addCocktail}
                            type="submit"
                            variant="contained"
                            color="secondary"
                            loading={loading}
                            disabled={loading}
                        >
                            Add cocktail
                        </ButtonWithProgress> :
                        ''}
                    {user && user.role !== 'admin' ?
                        <Grid item>
                            <Button color="primary" component={Link} to="?personal=true">My cocktails</Button>
                        </Grid>
                        : ''}
                </Grid>
                <Grid item>
                    <Grid item container justifyContent="center" direction="row" spacing={1}>
                        {fetchLoading ? (
                            <Grid container justifyContent="center" alignItems="center">
                                <Grid item>
                                    <CircularProgress/>
                                </Grid>
                            </Grid>
                        ) : cocktails.map(cocktail => (
                            <CocktailItem
                                key={cocktail._id}
                                id={cocktail._id}
                                title={cocktail.title}
                                recipe={cocktail.recipe}
                                ingredients={cocktail.ingredients}
                                image={cocktail.image}
                                published={cocktail.published}
                            />
                        ))}
                    </Grid>
                </Grid>
            </Grid>
        </CocktailLayout>
    );
};

export default Cocktails;