import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Typography} from "@material-ui/core";
import CocktailForm from "../../components/CocktailForm/CocktailForm";
import {createCocktail} from "../../store/actions/cocktailsActions";

const NewCocktail = () => {
    const dispatch = useDispatch();
    const error = useSelector(state => state.cocktails.createCocktailError);
    const loading = useSelector(state => state.cocktails.createCocktailLoading);

    const onSubmit = cocktailData => {
        dispatch(createCocktail(cocktailData));
    };

    return (
        <>
            <Typography variant="h4">New cocktail</Typography>
            <CocktailForm
                onSubmit={onSubmit}
                error={error}
                loading={loading}
            />
        </>
    );
};

export default NewCocktail;