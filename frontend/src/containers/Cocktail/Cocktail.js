import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchCocktail} from "../../store/actions/cocktailsActions";
import {Box, Paper, Typography} from "@material-ui/core";
import {apiURL} from "../../config";

const Cocktail = ({match}) => {
    console.log('test');
    const dispatch = useDispatch();
    const cocktail = useSelector(state => state.cocktails.cocktail);

    useEffect(() => {
        dispatch(fetchCocktail(match.params.id));
    }, [dispatch, match.params.id]);

    return cocktail && (
        <Paper component={Box} p={2}>
            <Typography variant="h4">{cocktail.title}</Typography>
            <img src={apiURL + '/' + cocktail.image} alt={cocktail.title} width="300" height="200"/>
            <Typography variant="body1"><i>{cocktail.recipe}</i></Typography>
            <Typography variant="body1"><b>Ingredients</b></Typography>
            {cocktail.ingredients.map((ingredient, i) => (
                <div key={i}>
                    <p>{ingredient.title} - {ingredient.amount}</p>
                </div>
            ))}
            <Typography variant="subtitle1">
                <b><i>{cocktail.published ? 'Опубликовано' : 'Не опубликовано'}</i></b>
            </Typography>
        </Paper>
    );
};

export default Cocktail;