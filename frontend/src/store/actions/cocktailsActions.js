import {toast} from "react-toastify";
import WarningIcon from '@material-ui/icons/Warning';
import axiosApi from "../../axiosApi";
import {historyPush} from "./historyActions";


export const FETCH_COCKTAILS_REQUEST = 'FETCH_COCKTAILS_REQUEST';
export const FETCH_COCKTAILS_SUCCESS = 'FETCH_COCKTAILS_SUCCESS';
export const FETCH_COCKTAILS_FAILURE = 'FETCH_COCKTAILS_FAILURE';

export const PUBLISH_COCKTAIL_REQUEST = 'PUBLISH_COCKTAIL_REQUEST';
export const PUBLISH_COCKTAIL_SUCCESS = 'PUBLISH_COCKTAIL_SUCCESS';
export const PUBLISH_COCKTAIL_FAILURE = 'PUBLISH_COCKTAIL_FAILURE';

export const FETCH_COCKTAIL_REQUEST = 'FETCH_COCKTAIL_REQUEST';
export const FETCH_COCKTAIL_SUCCESS = 'FETCH_COCKTAIL_SUCCESS';
export const FETCH_COCKTAIL_FAILURE = 'FETCH_COCKTAIL_FAILURE';

export const CREATE_COCKTAIL_REQUEST = 'CREATE_COCKTAIL_REQUEST';
export const CREATE_COCKTAIL_SUCCESS = 'CREATE_COCKTAIL_SUCCESS';
export const CREATE_COCKTAIL_FAILURE = 'CREATE_COCKTAIL_FAILURE';

export const DELETE_COCKTAIL_REQUEST = 'DELETE_COCKTAIL_REQUEST';
export const DELETE_COCKTAIL_SUCCESS = 'DELETE_COCKTAIL_SUCCESS';
export const DELETE_COCKTAIL_FAILURE = 'DELETE_COCKTAIL_FAILURE';

export const fetchCocktailsRequest = () => ({type: FETCH_COCKTAILS_REQUEST});
export const fetchCocktailsSuccess = cocktails => ({type: FETCH_COCKTAILS_SUCCESS, payload: cocktails});
export const fetchCocktailsFailure = () => ({type: FETCH_COCKTAILS_FAILURE});

export const publishCocktailRequest = () => ({type: PUBLISH_COCKTAIL_REQUEST});
export const publishCocktailSuccess = cocktail => ({type: PUBLISH_COCKTAIL_SUCCESS, payload: cocktail});
export const publishCocktailFailure = () => ({type: PUBLISH_COCKTAIL_FAILURE});

export const fetchCocktailRequest = () => ({type: FETCH_COCKTAIL_REQUEST});
export const fetchCocktailSuccess = cocktail => ({type: FETCH_COCKTAIL_SUCCESS, payload: cocktail});
export const fetchCocktailFailure = () => ({type: FETCH_COCKTAIL_FAILURE});

export const createCocktailRequest = () => ({type: CREATE_COCKTAIL_REQUEST});
export const createCocktailSuccess = () => ({type: CREATE_COCKTAIL_SUCCESS});
export const createCocktailFailure = (error) => ({type: CREATE_COCKTAIL_FAILURE, payload: error});

export const deleteCocktailRequest = () => ({type: DELETE_COCKTAIL_REQUEST});
export const deleteCocktailSuccess = (id) => ({type: DELETE_COCKTAIL_SUCCESS, payload: id});
export const deleteCocktailFailure = () => ({type: DELETE_COCKTAIL_FAILURE});

export const fetchCocktails = (query) => {
    return async (dispatch) => {
        try {
            dispatch(fetchCocktailsRequest());
            const response = await axiosApi.get('/cocktails' + query);
            dispatch(fetchCocktailsSuccess(response.data));
        } catch (error) {
            dispatch(fetchCocktailsFailure());
            toast.error('Could not fetch cocktails!', {
                theme: 'colored',
                icon: <WarningIcon/>
            });
        }
    }
};

export const publishCocktail = id => {
    return async (dispatch) => {
        try {
            dispatch(publishCocktailRequest());
            await axiosApi.post(`/cocktails/${id}/publish`);
            dispatch(publishCocktailSuccess(id));
            toast.success('Successfully published cocktail with id - ' + id);
        } catch (error) {
            if (error.response.status === 403) {
                toast.error('You don\'t have permission for this operation');
            }
            dispatch(publishCocktailFailure(error));
        }
    };
};

export const fetchCocktail = id => {
    return async dispatch => {
        try {
            dispatch(fetchCocktailRequest());
            const response = await axiosApi.get('http://localhost:8000/cocktails/' + id);
            dispatch(fetchCocktailSuccess(response.data));
        } catch (e) {
            dispatch(fetchCocktailFailure());
        }
    };
};

export const createCocktail = cocktailData => {
    return async dispatch => {
        try {
            dispatch(createCocktailRequest());
            await axiosApi.post('/cocktails', cocktailData);
            dispatch(createCocktailSuccess());
            dispatch(historyPush('/'));
            toast.success('Cocktail created and under consideration by the moderator');
        } catch (e) {
            dispatch(createCocktailFailure(e.response.data));
            toast.error('Could not create cocktail');
        }
    };
};

export const deleteCocktail = id => {
    return async (dispatch) => {
        try {
            dispatch(deleteCocktailRequest());
            await axiosApi.delete(`/cocktails/${id}`);
            dispatch(deleteCocktailSuccess(id));
            toast.success('Successfully deleted cocktail with id - ' + id);
        } catch (error) {
            if (error.response.status === 403) {
                toast.error('You don\'t have permission for this operation');
            }
            dispatch(deleteCocktailFailure(error));
        }
    };
};