import {
    CREATE_COCKTAIL_FAILURE,
    CREATE_COCKTAIL_REQUEST,
    CREATE_COCKTAIL_SUCCESS, DELETE_COCKTAIL_FAILURE,
    DELETE_COCKTAIL_REQUEST, DELETE_COCKTAIL_SUCCESS,
    FETCH_COCKTAIL_FAILURE,
    FETCH_COCKTAIL_REQUEST,
    FETCH_COCKTAIL_SUCCESS,
    FETCH_COCKTAILS_FAILURE,
    FETCH_COCKTAILS_REQUEST,
    FETCH_COCKTAILS_SUCCESS,
    PUBLISH_COCKTAIL_FAILURE,
    PUBLISH_COCKTAIL_REQUEST,
    PUBLISH_COCKTAIL_SUCCESS
} from "../actions/cocktailsActions";

const initialState = {
    cocktails: [],
    cocktail: null,
    fetchLoading: false,
    singleLoading: false,
    createCocktailLoading: false,
    createCocktailError: null
};

const cocktailsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_COCKTAILS_REQUEST:
            return {...state, fetchLoading: true};
        case FETCH_COCKTAILS_SUCCESS:
            return {...state, fetchLoading: false, cocktails: action.payload};
        case FETCH_COCKTAILS_FAILURE:
            return {...state, fetchLoading: false};
        case PUBLISH_COCKTAIL_REQUEST:
            return {...state, singleLoading: true};
        case PUBLISH_COCKTAIL_SUCCESS:
            const publishState = state.cocktails.filter(val => val._id === action.payload);
            publishState[0].published = true;
            const returnState = state.cocktails.filter(val => val._id !== action.payload);
            returnState.push(publishState[0]);
            return {...state, singleLoading: false, createCocktailError: null, cocktails: returnState};
        case PUBLISH_COCKTAIL_FAILURE:
            return {...state, singleLoading: false, createCocktailError: action.payload};
        case FETCH_COCKTAIL_REQUEST:
            return {...state, singleLoading: true};
        case FETCH_COCKTAIL_SUCCESS:
            return {...state, singleLoading: false, cocktail: action.payload};
        case FETCH_COCKTAIL_FAILURE:
            return {...state, singleLoading: false};
        case CREATE_COCKTAIL_REQUEST:
            return {...state, createCocktailLoading: true};
        case CREATE_COCKTAIL_SUCCESS:
            return {...state, createCocktailLoading: false, createCocktailError: null};
        case CREATE_COCKTAIL_FAILURE:
            return {...state, createCocktailLoading: false, createCocktailError: action.payload};
        case DELETE_COCKTAIL_REQUEST:
            return {...state, singleLoading: true};
        case DELETE_COCKTAIL_SUCCESS:
            const newState = state.cocktails.filter(val => val._id !== action.payload);
            return {...state, singleLoading: false, createCocktailError: null, cocktails: newState};
        case DELETE_COCKTAIL_FAILURE:
            return {...state, singleLoading: false};
        default:
            return state;
    }
};

export default cocktailsReducer;