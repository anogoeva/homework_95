import Layout from "./components/UI/Layout/Layout";
import {Route, Switch} from "react-router-dom";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import Cocktails from "./containers/Cocktails/Cocktails";
import NewCocktail from "./containers/NewCocktail/NewCocktail";
import Cocktail from "./containers/Cocktail/Cocktail";

const App = () => (
    <Layout>
        <Switch>
            <Route path="/" exact component={Cocktails}/>
            <Route path="/personal=true" component={Cocktails}/>
            <Route path="/cocktails/:id" component={Cocktail}/>
            <Route path="/addCocktail" component={NewCocktail}/>
            <Route path="/register" component={Register}/>
            <Route path="/login" component={Login}/>
        </Switch>
    </Layout>
);

export default App;
