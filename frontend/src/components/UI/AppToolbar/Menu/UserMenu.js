import React, {useState} from 'react';
import {Button, Menu, MenuItem} from "@material-ui/core";
import {useDispatch} from "react-redux";
import {logoutUser} from "../../../../store/actions/usersActions";
import imageNotAvailable from '../../../../assets/images/anonymous.png';


const UserMenu = ({user}) => {

  const dispatch = useDispatch();
  const [anchorEl, setAnchorEl] = useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

    let cardImage = imageNotAvailable;

    if (user.picture) {
        cardImage = user.picture;
    }

  return (
    <>
      <Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick} color="inherit">
          <img referrerPolicy="no-referrer" src={cardImage} alt="avatar" width="25" height="25"/>
        Hello, {user.displayName}!

      </Button>


      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <MenuItem onClick={() => dispatch(logoutUser())}>Logout</MenuItem>
      </Menu>
    </>
  );
};

export default UserMenu;