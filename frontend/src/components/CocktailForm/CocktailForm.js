import React, {useState} from "react";
import {Grid, makeStyles, TextField, Typography} from "@material-ui/core";
import FormElement from "../UI/Form/FormElement";
import ButtonWithProgress from "../UI/ButtonWithProgress/ButtonWithProgress";

const useStyles = makeStyles(theme => ({
    root: {
        marginTop: theme.spacing(2)
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

const CocktailForm = ({onSubmit, error, loading}) => {
    const classes = useStyles();

    const [ingredients, setIngredients] = useState([{title: '', amount: ''}]);

    const [cocktail, setCocktail] = useState({
        title: '',
        ingredients: JSON.stringify(ingredients),
        recipe: '',
        image: '',
    });

    const submitFormHandler = e => {
        console.log(JSON.stringify(ingredients));
        e.preventDefault();

        const formData = new FormData();
        Object.keys(cocktail).forEach(key => {
            formData.append(key, cocktail[key]);
        });

        onSubmit(formData);
    };

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setCocktail(prevState => {
            return {...prevState, [name]: value};
        });
    };

    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];
        setCocktail(prevState => {
            return {...prevState, [name]: file};
        });
    };

    const getFieldError = fieldName => {
        try {
            return error.errors[fieldName].message;
        } catch (e) {
            return undefined;
        }
    };


    const changeIngredient = (i, name, value) => {
        setIngredients(prev => {
            const ingCopy = {
                ...prev[i],
                [name]: value,
            };
            return prev.map((ing, index) => {
                if (i === index) {
                    return ingCopy;
                }
                return ing;
            });
        });
    };

    const addIngredients = () => {
        setIngredients(prev => [
            ...prev,
            {title: '', amount: ''}
        ]);
    };

    const deleteIngredient = (index) => {
        setIngredients(ingredients.filter((ing, i) => i !== index));
    };

    return (
        <Grid
            container
            direction="column"
            spacing={2}
            component="form"
            className={classes.root}
            autoComplete="off"
            onSubmit={submitFormHandler}
            noValidate>
            <Typography variant="h4">Add new cocktail</Typography>
            <Grid>
                <FormElement
                    required
                    label="Cocktail name"
                    name="title"
                    value={cocktail.title}
                    onChange={inputChangeHandler}
                    error={getFieldError('title')}/>
                <br/>
                <Typography variant="h6">Ingredients</Typography>
                <br/>
                {ingredients.map((ing, i) => (
                    <Grid key={i}>
                        <FormElement
                            required
                            label="Ingredient name"
                            name="ingredient name"
                            value={ing.title}
                            onChange={e => changeIngredient(i, 'title', e.target.value)}
                            error={getFieldError('ingredient name')}/>
                        <br/>
                        <FormElement
                            required
                            label="Amount"
                            name="amount"
                            value={ing.amount}
                            onChange={e => changeIngredient(i, 'amount', e.target.value)}
                            error={getFieldError('amount')}/>
                        <button onClick={() => deleteIngredient(i)} type="button">Delete</button>
                        <hr/>
                        <br/>
                    </Grid>
                ))}
                <button type="button" onClick={addIngredients}>Add ingredients</button>
                <hr/>
                <FormElement
                    required
                    multiline
                    rows={4}
                    label="Recipe"
                    name="recipe"
                    value={cocktail.recipe}
                    onChange={inputChangeHandler}
                    error={getFieldError('recipe')}/>
                <Grid item xs>
                    <TextField
                        type="file"
                        name="image"
                        onChange={fileChangeHandler}
                        error={Boolean(getFieldError('image'))}
                        helperText={getFieldError('image')}
                    />
                </Grid>
                <Grid item xs={12}>
                    <ButtonWithProgress
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                        loading={loading}
                        disabled={loading}
                    >
                        Create cocktail
                    </ButtonWithProgress>
                </Grid>
            </Grid>
        </Grid>
    );
};

export default CocktailForm;