import {
    Button,
    Card,
    CardActions,
    CardContent,
    CardHeader,
    CardMedia,
    Grid,
    IconButton,
    makeStyles,
} from "@material-ui/core";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import PropTypes from 'prop-types';
import {Link} from "react-router-dom";
import imageNotAvailable from '../../assets/images/not_available.png';
import {apiURL} from "../../config";
import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {deleteCocktail, publishCocktail} from "../../store/actions/cocktailsActions";

const useStyles = makeStyles({
    card: {
        height: '100%'
    },
    media: {
        height: 0,
        paddingTop: '56.25%'
    }
});

const CocktailItem = ({id, title, image, published}) => {

    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);
    const classes = useStyles();

    let cardImage = imageNotAvailable;

    if (image) {
        cardImage = apiURL + '/' + image;
    }

    const publish = (id) => {
        dispatch(publishCocktail(id));
    };

    const deleteHandler = (id) => {
        dispatch(deleteCocktail(id));
    };

    return (
        <Grid item xs={12} sm={12} md={6} lg={4}>
            <Card className={classes.card}>
                <CardHeader title={title}/>
                <CardMedia
                    image={cardImage}
                    title={title}
                    className={classes.media}
                />
                <CardContent><br/>
                    {user && user.role === 'admin' ?
                        <Button type="submit" onClick={() => publish(id)}>
                            <b>{published ? '' : 'Опубликовать'}</b>
                        </Button>
                        : ''}
                    {user && user.role === 'admin' ?
                        <Button type="submit" onClick={() => deleteHandler(id)}>
                            <b>Delete</b>
                        </Button>
                        : ''}
                </CardContent>
                <CardActions>
                    <IconButton component={Link} to={'/cocktails/' + id}>
                        <ArrowForwardIcon/>
                    </IconButton>
                </CardActions>
            </Card>
        </Grid>
    );
};

CocktailItem.propTypes = {
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    recipe: PropTypes.string.isRequired,
    image: PropTypes.string,
    published: PropTypes.bool,
    ingredients: PropTypes.array,
};

export default CocktailItem;